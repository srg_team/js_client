// var socket = new WebSocket('');
var socket = new WebSocket("ws://84.237.53.148:9000");
var List1 = $('#listitem_holder1');
var List2 = $('#listitem_holder2');

var ready = {
    'type' : 'ready',
    'data':{}
};
var createLobbyRequest = {
    'type' : 'createlobby',
    'data' : {}
};


localStorage.setItem("SRG_login_info", JSON.stringify({
    'type' : 'login',
    'data':{
        'username':'user'
    }
})); // это костыль , тк пока не разработан бекенд


var loginJson = localStorage.getItem("SRG_login_info");

$('#ready_btn').on('click', function (e) {
    e.target.classList.remove('btn-primary');
    e.target.classList.add('bg-success');
    socket.send(JSON.stringify(ready));
});

socket.onmessage = function (event) {
    console.log('was received ' + event.data);
    var received = JSON.parse(event.data);
    if(received.type === 'game'){
        showRequests(received.data.requests);
        //display gamefield and rival
    }
    if(received.type === 'endgame'){
        alert(`you picked request \'`
            + received.data[0].request
            + '\'\nYour score : '
            + received.data[0].score);
    }
    if(received.type === 'error'){
        if(received.data === 'You already have active game'){

            // socket.send(JSON.stringify({
            //     'type' : 'game',
            //     'data' : {}
            // }));
            // socket.send(JSON.stringify({
            //     'type' : 'lobby',
            //     'data' : {}
            // }));

        }
    }
    if(received.type === 'lobby'){
        if(received.data.players.length > 1){
            console.log('we have rival here....');
            if(received.data.players[0].username === JSON.parse(localStorage.getItem('SRG_login_info')).data.username){
                $('#rival_username').text(received.data.players[1].username);
            }else {
                $('#rival_username').text(received.data.players[0].username);
            }
        }
    }
    // if(event.type == 'playerinfo'){
        //сохранить информацию о своем игроке
    // }
    //принимаю имя и аву свои, противника, список слов

};
socket.onopen = function(){
    console.log("Connected!\n");
    console.log('going to send login');
    $('#username_main').text(JSON.parse(loginJson).data.username);
    $('#my_username').text(JSON.parse(loginJson).data.username);
    socket.send(loginJson);
    // socket.send(JSON.stringify({
    //     'type' : 'pickrequest',
    //     'data' : {'requestid' : 1}
    // }));
    // return;
    console.log('going to send createlobby request');
    socket.send(JSON.stringify(createLobbyRequest));
    console.log('going to send ready');
    // socket.send(JSON.stringify(ready));

};

// console.log(JSON.stringify(ready));
// socket.send(JSON.stringify(ready));


function showRequests(requests) {
    console.log('displaying requests..');
    var count = requests.length;
    var flag = true;
    while (List1.firstChild) {
        List1.removeChild(List1.firstChild);
    }
    while (List2.firstChild) {
        List2.removeChild(List2.firstChild);
    }
    while (count > 0){
        var a = $('<a></a>').addClass('list-group-item list-group-item-action game-request')
            .attr('data-id', requests[count-1].id)
            .attr('href', '#')
            .text(requests[count-1].request);
            if(requests[count-1].isPicked){
                a.classList.add('active disabled');
            }
        if (flag){
            List1.append(a);
        }else {
            List2.append(a);
        }
        flag = !flag;
        count--;
    }
    $('a.game-request').on('click', function (e) {
        var a = e.target;
        a.classList.add('active');
        var pickrequest = JSON.stringify({
            'type' : 'pickrequest',
            'data' : {'requestid' : parseInt(a.dataset.id)}
        });
        socket.send(pickrequest);
        $('a.game-request').addClass('disabled');
    });
}


$('#submitUsername').on('click', function () {
    var username = $('#form-name')[0].value;
    console.log(username);
    localStorage.setItem("SRG_login_info", JSON.stringify({
        'type' : 'login',
        'data':{
            'username':username
        }
    }));
    $('#username_main').text(username);
});
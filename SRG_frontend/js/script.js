var socket = new WebSocket("ws://84.237.53.148:9000");
socket.onopen = function() {
    // alert("Соединение установлено.");
};
socket.onmessage = function (event) {
    // alert('[message] Data received from server:' , );
    var json = JSON.parse(event.data);
    showLobbies(json);
    console.log(json);
};

// $('#quick_start').on('click', function () {
//     socket.close();
// });

var List =$('#lobby_list');
function showLobbies(lobbyes) {
    console.log("start showing lobbies...");
    var storage_size = lobbyes.length;
    if(storage_size > 0){
        for (var i = 0; i < storage_size; i++) {
            var lobby = lobbyes[i];
            console.log('lobby '+i+' -- ' +  lobby);
            var a = $('<a></a>').addClass('list-group-item')
                .attr('href', '/lobby')
                .appendTo(List);
            var container = $('<div></div>').addClass('d-flex w-100 justify-content-between')
                .appendTo(a);
            var profile = $('<div></div>').addClass('profile')
                .appendTo(container);
            container.append(profile);
            var avatar = $('<img>').attr('src', lobby.__photo__)
                .attr('alt', 'avatar')
                .appendTo(profile);
             $('<p></p>').addClass('username text-center')
                .text(lobby.players[0].username)
                .appendTo(profile);

             $('<div></div>').addClass('imageholder')
                .append($('<img>').attr('src', '../images/vs.png')
                    .attr('alt', 'icon \"versus\"'))
                .appendTo(container);

              $('<div></div>').addClass('imageholder')
                .append($('<img>').attr('src', '../images/plus_icon.png')
                    .attr('alt', 'place for player'))
                .append($('<p></p>').addClass('join_place text-center').text('Join'))
                .appendTo(container);


        }
    }
}
